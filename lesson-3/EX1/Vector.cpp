#include "Vector.h"
#include <iostream>

/*creates vector with size n of elements
if n smaller than 2 capasity will be 2*/
Vector::Vector(int n)
{
	if (n < 2)
	{
		this->_elements = new int[2]{0};
		this->_capacity = 2;
	}
	else
	{
		this->_elements = new int[n] {0};
		this->_capacity = n;
	}
	this->_resizeFactor = _capacity;
	this->_size = 0;
}

Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}

int Vector::size() const
{
	return _size;
}

int Vector::capacity() const
{
	return _capacity;
}

int Vector::resizeFactor() const
{
	return _resizeFactor;
}

bool Vector::empty() const
{
	
	return !_size;
}

/*function adds an element to end of elements array*/
void Vector::push_back(const int & val)
{
	if (_size == _capacity) //in case array is full
	{
		reserve(_capacity + _resizeFactor);
	}
	
	this->_elements[_size] = val;
	this->_size += 1;
	
}

/*function returns last element in array and deletes it*/
int Vector::pop_back()
{
	int toReturn = 0;
	if (_size == 0) //in case there are no elements
	{
		std::cout << "error: pop from empty vector" << std::endl;
		toReturn = -9999;
	}
	else
	{
		toReturn = _elements[_size - 1];
		this->_elements[_size - 1] = 0; //deletes element
		this->_size -= 1;
	}
	
	return toReturn;
}

/*function makes sure vector has at least n capacity
if not it adds place to elements and changes the capacity*/
void Vector::reserve(int n)
{
	if (n > _capacity)
	{
		
		int toAdd = (n - _capacity) / _resizeFactor;
		if ((n - _capacity) % _resizeFactor != 0)
		{
			toAdd++;
		}
		this->_capacity += (_resizeFactor*toAdd);
		int* elements = new int[_capacity];
		for (int i = 0; i < _size; i++)
		{
			elements[i] = _elements[i];
		}
		delete[] this->_elements;
		this->_elements = new int[_capacity];
		this->_elements = elements;
	}
	
}

/*function makes sure that there are n elements in vector*/
void Vector::resize(int n)
{
	if (n < _capacity)
	{
		for (int i = n; i < _capacity; i++)
		{
			this->_elements[i] = 0; //"delete" extra elements
		}
	}
	else
	{
		reserve(n);
	}
	this->_size = n;
}

/*changes all values available to value given*/
void Vector::assign(int val)
{
	for (int i = _size; i < _capacity; i++)
	{
		this->_elements[i] = val;
	}
	this->_size = _capacity; //no available space left
}

/*changes size of vector to n and if n bigger than size 
elements with value val added*/
void Vector::resize(int n, const int & val)
{
	int size = 0;
	if (n > _capacity)
	{
		size = _size;
		resize(n);
		for (int i = size; i < n; i++)
		{
			this->_elements[i] = val;
		}
	}
	else
	{
		resize(n);
	}
	
}

/*creates a vector with parameters from other vector*/
Vector::Vector(const Vector & other)
{
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_elements = new int[_capacity] {0};
	for (int i = 0; i < _size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	
}

/*puts values of given vector in vector (overwriting operator =)*/
Vector & Vector::operator=(const Vector & other)
{
	if (this == &other)
	{
		return *this;
	}
	
	this->~Vector();
	this->Vector::Vector(other);
	return *this;
}

/*returns a value in place n in vector*/
int & Vector::operator[](int n) const
{
	if (n > _size || n < 0)
	{
		std::cout << "n exceeds the boundaries of a vector, first element is returned" << std::endl;
		return _elements[0];
	}
	else
	{
		return _elements[n];
	}
}

/*overwriting operator + in order to add values of operators to each other*/
Vector  operator+(const Vector & v1, const Vector& v2)
{
	Vector v3(v1);
	if (v2._size < v1._size)
	{
		v3 = v2;
		for (int i = 0; i < v3._size; i++)
		{
			v3._elements[i] += v1._elements[i];
		}
	}
	else
	{
		for (int i = 0; i < v3._size; i++)
		{
			v3._elements[i] += v2._elements[i];
		}
	}
	
	
	return v3;
}

/*overwriting operator - in order to substract values of operators to each other*/
Vector operator-(const Vector & v1, const Vector& v2)
{
	Vector v3(v1);
	if (v2._size < v1._size)
	{
		v3 = v2;
		for (int i = 0; i < v3._size; i++)
		{
			v3._elements[i] -= v1._elements[i];
		}
	}
	else
	{
		for (int i = 0; i < v3._size; i++)
		{
			v3._elements[i] -= v2._elements[i];
		}
	}


	return v3;
}

/*overwriting operator << to be able to print vector using <<*/
std::ostream & operator<<(std::ostream & os, const Vector & vector)
{
	os << "Vector Info:\n" << "Capacity is " << vector._capacity << "\nSize is " << vector._size << "\ndata is ";
	for (int i = 0; i < vector._size; i++)
	{
		os << vector._elements[i];
		os << " ";
	}
	os << "\n";
	return os;
}
